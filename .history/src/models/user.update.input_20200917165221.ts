import { Field, ID, InputType } from '@nestjs/graphql';
import { IsEmail, IsPhoneNumber } from 'class-validator';

@InputType()
export class UserUpdate {
  @Field({ nullable: true })
  client_id: string;

  @Field({ nullable: true })
  active: boolean;

  @Field({ nullable: true })
  title: string;

  @Field({ nullable: true })
  first_name: string;

  @Field({ nullable: true })
  last_name: string;

  @Field({ nullable: false })
  @IsEmail()
  email: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  transactions: number;

  // @Field({ nullable: false, defaultValue: 'ap' })
  @Field({ nullable: false })
  tier: string;

  @Field({ nullable: true })
  last_seen: Date;

  @Field({ nullable: false })
  password: string;

  @Field({ nullable: false })
  inserted_at: Date;

  @Field({ nullable: false, defaultValue: new Date() })
  updated_at: Date;

  @Field({ nullable: true, defaultValue: 'password' })
  provider: string;

  @Field(() => [String], { nullable: true })
  recent_viewed: [];

  @Field(() => [String], { nullable: true })
  favourites: [];

  @Field(() => [String], { nullable: true })
  products_saved_for_later: [];

  @Field({ nullable: true, defaultValue: false })
  is_alliance: boolean;

  @Field({ nullable: true })
  alliance_id: string;

  @Field({ nullable: true })
  oauth_id: string;

  @Field({ nullable: true })
  phone_country_code: string;

  @Field({ nullable: true })
  city_id: number;

  @Field({ nullable: true })
  country_id: number;

  @Field({ nullable: true })
  news_letter_subscribed: boolean;

  @Field({ nullable: true })
  vat_number: string;

  @Field({ nullable: true })
  vat_registration_date: Date;
}
