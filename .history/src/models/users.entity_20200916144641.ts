import { Any, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType, InputType } from '@nestjs/graphql';

@Entity()
@ObjectType()
@InputType()
export class Users {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  @Field(() => ID)
  id: bigint;

  @Column({ length: 255, nullable: true })
  @Field()
  client_id: string;

  @Column({ default: true, nullable: true })
  @Field()
  active: boolean;

  @Column({ length: 255, nullable: true })
  @Field()
  title: string;

  @Column({ length: 255, nullable: true })
  @Field()
  first_name: string;

  @Column({ length: 255, nullable: true })
  @Field()
  last_name: string;

  @Column({ nullable: false, length: 255 })
  @Field()
  email: string;

  @Column({ length: 255, nullable: true })
  @Field()
  phone: string;

  @Column({ nullable: true })
  @Field()
  transactions: number;

  @Column({ nullable: false, default: 'ap', length: 255 })
  @Field()
  tier: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: true,
  })
  @Field()
  last_seen: Date;

  @Column({ nullable: false, length: 255 })
  @Field()
  password: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: false,
  })
  @Field()
  inserted_at: Date;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: false,
  })
  @Field()
  updated_at: Date;

  @Column({ nullable: true, length: 255 })
  @Field()
  provider: string;

  @Column({
    type: 'jsonb',
    default: () => "'{}'",
    nullable: true,
  })
  @Field(() => [String])
  recent_viewed: [];

  @Column({ type: 'jsonb', default: () => "'{}'", nullable: true })
  @Field(() => [String])
  favourites: [];

  @Column({ type: 'jsonb', default: () => "'{}'", nullable: true })
  @Field(() => [String])
  products_saved_for_later: [];

  @Column({ default: false, nullable: true })
  @Field()
  is_alliance: boolean;

  @Column({ length: 255, nullable: true })
  @Field()
  alliance_id: string;

  @Column({ length: 255, nullable: true })
  @Field()
  oauth_id: string;

  @Column({ length: 11, nullable: true })
  @Field()
  phone_country_code: string;

  @Column({ nullable: true })
  @Field()
  city_id: number;

  @Column({ nullable: true })
  @Field()
  country_id: number;

  @Column({ nullable: true })
  @Field()
  news_letter_subscribed: boolean;

  @Column({ length: 255, nullable: true })
  @Field()
  vat_number: string;

  @Column({ nullable: true })
  @Field()
  vat_registration_date: Date;
}
