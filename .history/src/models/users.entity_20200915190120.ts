import { Column, Entity } from 'typeorm';

@Entity()
export class Users {
    @Column({default: true})
    active:boolean;

    @Column()
    client_id:string;

    @Column()
    title:string;

    @Column()
    first_name:string;

    @Column()
    last_name:string;

    @Column({nullable:false})
    email:string;

    @Column({nullable:false})
    provider:string;

    @Column()
    oauth_id:string;

    @Column()
    phone:string;

    @Column()
    transactions:number;

    @Column({nullable:false,default:"ap"})
    tier:string;

    @Column({type:"timestamp",default:Date.now()})
    last_seen:Date;

    @Column({nullable:false})
    password:string;

    @Column({default:false})
    is_alliance:boolean;

    @Column()
    alliance_id:string;

    @Column({type:"_jsonb",default:[]})
    recent_viewed: [];

    @Column()
    favourites, {:array:map}, default: []

    @Column()
    products_saved_for_later, {:array:map}, default: []

    @Column()
    phone_country_code:string

    @Column()
    city_id:number

    @Column()
    country_id:number

    @Column()
    vat_number:string

    @Column()
    vat_registration_date:date

    @Column()
    news_letter_subscribed:boolean


    // Adding this to avoid failures in other endpoints where we donot require store data!
    @Column()
    store:map, virtual: true

}
