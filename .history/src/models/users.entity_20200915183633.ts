import { Column, Entity } from 'typeorm';

@Entity()
export class Users {
    @Column({default: true})
    active:boolean;
    field :client_id, :string
    field :title, :string
    field :first_name, :string
    field :last_name, :string
    field :email, :string, null: false
    field :provider, :string, null: false
    field :oauth_id, :string
    field :phone, :string
    field :transactions, :integer
    field :tier, :string, null: false, default: "ap"
    field :last_seen, Timex.Ecto.DateTime, default: Timex.now
    field :password, :string, null: false
    field :is_alliance, :boolean, default: false
    field :alliance_id, :string
    field :recent_viewed, {:array, :map}, default: []
    field :favourites, {:array, :map}, default: []
    field :products_saved_for_later, {:array, :map}, default: []
    field :phone_country_code, :string
    field :city_id, :integer
    field :country_id, :integer
    field :vat_number, :string
    field :vat_registration_date, :date
    field :news_letter_subscribed, :boolean

    // Adding this to avoid failures in other endpoints where we donot require store data!
    field :store, :map, virtual: true
    has_many :user_addresses, Paints.UserAddress, on_delete: :delete_all, on_replace: :delete
    has_one :painter, Paints.Schema.Painter, on_delete: :delete_all, on_replace: :delete
    belongs_to :data_areas, Paints.DataArea, [foreign_key: :data_area_id]
    timestamps()
}
