import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Admins {
    @PrimaryGeneratedColumn()
    @Field()
    id:bigint;

    @Column({length:255})
    @Field()
    first_name:string;

 @Column({length:255})
@Field()
 last_name:string;
 
 email           | character varying(255)      |           | not null | 
 active          | boolean                     |           |          | true
 password        | character varying(255)      |           | not null | 
 inserted_at     | timestamp without time zone |           | not null | 
 updated_at      | timestamp without time zone |           | not null | 
 webhook_token   | character varying(255)      |           |          | 
 superadmin      | boolean                     |           |          | 
 permissions     | jsonb                       |           |          | 
 cities_area_ids |
}
