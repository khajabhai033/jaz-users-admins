import { Any, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType, InputType } from '@nestjs/graphql';

@Entity()
@ObjectType()
@InputType('UserInput')
export class Users {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  @Field(() => ID, { nullable: true })
  id: bigint;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  client_id: string;

  @Column({ default: true, nullable: true })
  @Field({ nullable: true })
  active: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  title: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  first_name: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  last_name: string;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  email: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  transactions: number;

  @Column({ nullable: false, default: 'ap', length: 255 })
  @Field({ nullable: false })
  tier: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: true,
  })
  @Field({ nullable: true })
  last_seen: Date;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  password: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: false,
  })
  @Field({ nullable: false })
  inserted_at: Date;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: false,
  })
  @Field({ nullable: false })
  updated_at: Date;

  @Column({ nullable: true, length: 255 })
  @Field({ nullable: true })
  provider: string;

  @Column({
    type: 'jsonb',
    default: () => "'{}'",
    nullable: true,
  })
  @Field(() => [String], { nullable: true })
  recent_viewed: [];

  @Column({ type: 'jsonb', default: () => "'{}'", nullable: true })
  @Field(() => [String], { nullable: true })
  favourites: [];

  @Column({ type: 'jsonb', default: () => "'{}'", nullable: true })
  @Field(() => [String], { nullable: true })
  products_saved_for_later: [];

  @Column({ default: false, nullable: true })
  @Field({ nullable: true })
  is_alliance: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  alliance_id: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  oauth_id: string;

  @Column({ length: 11, nullable: true })
  @Field({ nullable: true })
  phone_country_code: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  city_id: number;

  @Column({ nullable: true })
  @Field({ nullable: true })
  country_id: number;

  @Column({ nullable: true })
  @Field({ nullable: true })
  news_letter_subscribed: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  vat_number: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  vat_registration_date: Date;
}
