import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Admins {
  @PrimaryGeneratedColumn()
  @Field()
  id: bigint;

  @Column({ length: 255 })
  @Field()
  first_name: string;

  @Column({ length: 255 })
  @Field()
  last_name: string;

  @Column({ length: 255 })
  @Field()
  email: string;

  @Column({ default: true })
  @Field()
  active: boolean;

  @Column({ length: 255 })
  @Field()
  password: string;

  @Column({ type: 'timestamp without time zone' })
  @Field()
  inserted_at: Date;

  @Column({ type: 'timestamp without time zone' })
  @Field()
  updated_at: Date;

  @Column({ length: 255, nullable: true })
  @Field()
  webhook_token: string;

  @Column({ nullable: true })
  @Field()
  superadmin: boolean;

  @Column({ type: 'jsonb', nullable: true })
  @Field()
  permissions: any;

  @Column({ type: 'int4range' })
  @Field()
  cities_area_ids: any;
}
