import { Field, ID, InputType } from '@nestjs/graphql';
import { IsEmail } from 'class-validator';

@InputType()
export class User {
  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  client_id: string;

  @Column({ default: true, nullable: true })
  @Field({ nullable: true })
  active: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  title: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  first_name: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  last_name: string;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  email: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  transactions: number;

  @Column({ nullable: false, default: 'ap', length: 255 })
  @Field({ nullable: false, defaultValue: 'ap' })
  tier: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: true,
  })
  @Field({ nullable: true })
  last_seen: Date;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  password: string;

  @Column({
    type: 'timestamp without time zone',
    nullable: false,
  })
  @Field({ nullable: false })
  inserted_at: Date;

  @Field({ nullable: false, defaultValue: new Date() })
  updated_at: Date;

  @Field({ nullable: true })
  provider: string;

  @Field(() => [String], { nullable: true })
  recent_viewed: [];

  @Field(() => [String], { nullable: true })
  favourites: [];

  @Field(() => [String], { nullable: true })
  products_saved_for_later: [];

  @Field({ nullable: true, defaultValue: false })
  is_alliance: boolean;

  @Field({ nullable: true })
  alliance_id: string;

  @Field({ nullable: true })
  oauth_id: string;

  @Field({ nullable: true })
  phone_country_code: string;

  @Field({ nullable: true })
  city_id: number;

  @Field({ nullable: true })
  country_id: number;

  @Field({ nullable: true })
  news_letter_subscribed: boolean;

  @Field({ nullable: true })
  vat_number: string;

  @Field({ nullable: true })
  vat_registration_date: Date;
}
