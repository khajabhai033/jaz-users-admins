import {
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import {
  Field,
  ID,
  ObjectType,
  Int,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import * as jwt from 'jsonwebtoken';
import { UserProductDto } from '../types/user.products.dto';

@Entity()
@ObjectType()
export class Users {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  @Field(() => ID)
  id: bigint;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  client_id: string;

  @Column({ default: true, nullable: true })
  @Field({ nullable: true })
  active: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  title: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  first_name: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  last_name: string;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  email: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  transactions: number;

  @Column({ nullable: false, default: 'ap', length: 255 })
  @Field({ nullable: false, defaultValue: 'ap' })
  tier: string;

  @Column({
    type: 'timestamp without time zone',
    default: 'NOW()',
    nullable: true,
  })
  @Field(type => GraphQLISODateTime, { nullable: true })
  last_seen: Date;

  @Column({ nullable: false, length: 255 })
  @Field({ nullable: false })
  password: string;

  @Column({
    type: 'timestamp without time zone',
    nullable: false,
  })
  @CreateDateColumn()
  @Field(type => GraphQLISODateTime, { nullable: false })
  inserted_at: Date;

  @Column({
    type: 'timestamp without time zone',
    nullable: false,
  })
  @UpdateDateColumn()
  @Field(type => GraphQLISODateTime, { nullable: false })
  updated_at: Date;

  @Column({ nullable: true, length: 255 })
  @Field({ nullable: true })
  provider: string;

  @Column({
    type: 'jsonb',
    array: true,
    default: () => "'{}'",
    nullable: true,
  })
  @BeforeUpdate()
  @Field(() => [UserProductDto], { nullable: true })
  @Reflect.metadata('design:type', UserProductDto)
  recent_viewed: UserProductDto[];

  @Column({ type: 'jsonb', array: true, default: () => "'{}'", nullable: true })
  @Reflect.metadata('design:type', Object)
  @Field(() => [UserProductDto], { nullable: true })
  favourites: UserProductDto[];

  @Column({ type: 'jsonb', array: true, default: () => "'{}'", nullable: true })
  @Field(() => [UserProductDto], { nullable: true })
  @Reflect.metadata('design:type', Object)
  products_saved_for_later: UserProductDto[];

  @Column({ default: false, nullable: true })
  @Field({ nullable: true })
  is_alliance: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  alliance_id: string;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  oauth_id: string;

  @Column({ length: 11, nullable: true })
  @Field({ nullable: true })
  phone_country_code: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  city_id: number;

  @Column({ nullable: true })
  @Field({ nullable: true })
  country_id: number;

  @Column({ nullable: true })
  @Field({ nullable: true })
  news_letter_subscribed: boolean;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  vat_number: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  vat_registration_date: Date;

  public async generateToken(): Promise<string> {
    return await jwt.sign(
      { id: this.id, email: this.email },
      process.env.PRIVATE_KEY,
    );
  }

  @BeforeUpdate()
  async updatePassword(): Promise<void> {
    console.log(this.recent_viewed);
  }
}
