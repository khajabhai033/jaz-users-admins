import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@Entity()
@ObjectType()
export class Users {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: number;

  @Column()
  client_id: string;

  @Column({ default: true })
  active: boolean;

  @Column()
  title: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column({ nullable: false })
  email: string;

  @Column()
  phone: string;

  @Column()
  transactions: number;

  @Column({ nullable: false, default: 'ap' })
  tier: string;

  @Column({ type: 'timestamp', default: Date.now() })
  last_seen: Date;

  @Column({ nullable: false })
  password: string;

  @Column({ type: 'timestamp', default: Date.now() })
  inserted_at: Date;

  @Column({ type: 'timestamp', default: Date.now() })
  updated_at: Date;

  @Column({ nullable: false })
  provider: string;

  @Column({ type: 'jsonb', default: [] })
  recent_viewed: [];

  @Column({ type: 'jsonb', default: [] })
  favourites: [];

  @Column({ type: 'jsonb', default: [] })
  products_saved_for_later: [];

  @Column({ default: false })
  is_alliance: boolean;

  @Column()
  alliance_id: string;

  @Column()
  oauth_id: string;

  @Column()
  phone_country_code: string;

  @Column()
  city_id: number;

  @Column()
  country_id: number;

  @Column()
  news_letter_subscribed: boolean;

  @Column()
  vat_number: string;

  @Column()
  vat_registration_date: Date;
}
