import { Column, Entity } from 'typeorm';

@Entity()
export class Users {
    @Column({default: true})
    active:boolean;

    @Column()
    client_id:string;

    @Column()
    title:string;

    @Column()
    first_name:string;

    @Column()
    last_name:string;

    @Column({nullable:false})
email:string; null: false
    @Column({nullable:false})
provider:string;
    @Column()
oauth_id:string
    @Column()
phone:string
    @Column()
transactions:number
    @Column()
tier:string, null: false, default: "ap"
    @Column()
last_seen, Timex.Ecto.DateTime, default: Timex.now
    @Column()
password:string, null: false
    @Column()
is_alliance:boolean, default: false
    @Column()
alliance_id:string
    @Column()
recent_viewed, {:array:map}, default: []
    @Column()
favourites, {:array:map}, default: []
    @Column()
products_saved_for_later, {:array:map}, default: []
    @Column()
phone_country_code:string
    @Column()
city_id:number
    @Column()
country_id:number
    @Column()
vat_number:string
    @Column()
vat_registration_date:date
    @Column()
news_letter_subscribed:boolean

    // Adding this to avoid failures in other endpoints where we donot require store data!
    @Column()
store:map, virtual: true
    has_many :user_addresses, Paints.UserAddress, on_delete: :delete_all, on_replace: :delete
    has_one :painter, Paints.Schema.Painter, on_delete: :delete_all, on_replace: :delete
    belongs_to :data_areas, Paints.DataArea, [foreign_key: :data_area_id]
    timestamps()
}
