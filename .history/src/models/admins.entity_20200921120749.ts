import { Field, Int, ObjectType } from '@nestjs/graphql';
import { AdminPermission } from '../types/adminPermissions.dto';
import {
  Any,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@ObjectType()
@Entity()
export class Admins {
  @PrimaryGeneratedColumn()
  @Field(() => Int)
  id: bigint;

  @Column({ length: 255 })
  @Field()
  first_name: string;

  @Column({ length: 255 })
  @Field()
  last_name: string;

  @Column({ length: 255 })
  @Field()
  email: string;

  @Column({ default: true })
  @Field()
  active: boolean;

  @Column({ length: 255 })
  @Field()
  password: string;

  @Column({ type: 'timestamp without time zone' })
  @CreateDateColumn()
  @Field()
  inserted_at: Date;

  @Column({ type: 'timestamp without time zone' })
  @UpdateDateColumn()
  @Field()
  updated_at: Date;

  @Column({ length: 255, nullable: true })
  @Field({ nullable: true })
  webhook_token: string;

  @Column({ nullable: true })
  @Field()
  superadmin: boolean;

  @Column({ type: 'jsonb', nullable: true })
  @Field(() => AdminPermission)
  permissions: AdminPermission;

  @Column({ type: 'int4range' })
  @Field(() => [Int])
  cities_area_ids: any;
}
