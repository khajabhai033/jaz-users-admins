import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@Entity()
@ObjectType()
export class Users {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: number;

  @Column()
  @Field()
  client_id: string;

  @Column({ default: true })
  @Field()
  active: boolean;

  @Column()
  @Field()
  title: string;

  @Column()
  @Field()
  first_name: string;

  @Column()
  @Field()
  last_name: string;

  @Column({ nullable: false })
  @Field()
  email: string;

  @Column()
  @Field()
  phone: string;

  @Column()
  @Field()
  transactions: number;

  @Column({ nullable: false, default: 'ap' })
  @Field()
  tier: string;

  @Column({ type: 'timestamp', default: Date.now() })
  @Field()
  last_seen: Date;

  @Column({ nullable: false })
  @Field()
  password: string;

  @Column({ type: 'timestamp', default: Date.now() })
  @Field()
  inserted_at: Date;

  @Column({ type: 'timestamp', default: Date.now() })
  @Field()
  updated_at: Date;

  @Column({ nullable: false })
  @Field()
  provider: string;

  @Column({ type: 'jsonb', default: [] })
  @Field()
  recent_viewed: [];

  @Column({ type: 'jsonb', default: [] })
  @Field()
  favourites: [];

  @Column({ type: 'jsonb', default: [] })
  @Field()
  products_saved_for_later: [];

  @Column({ default: false })
  @Field()
  is_alliance: boolean;

  @Column()
  @Field()
  alliance_id: string;

  @Column()
  @Field()
  oauth_id: string;

  @Column()
  @Field()
  phone_country_code: string;

  @Column()
  @Field()
  city_id: number;

  @Column()
  @Field()
  country_id: number;

  @Column()
  @Field()
  news_letter_subscribed: boolean;

  @Column()
  @Field()
  vat_number: string;

  @Column()
  @Field()
  vat_registration_date: Date;
}
