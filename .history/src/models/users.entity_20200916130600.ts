import { Any, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@Entity()
@ObjectType()
export class Users {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  @Field(() => ID)
  id: bigint;

  @Column({ length: 255 })
  @Field()
  client_id: string;

  @Column({ default: true })
  @Field()
  active: boolean;

  @Column({ length: 255 })
  @Field()
  title: string;

  @Column({ length: 255 })
  @Field()
  first_name: string;

  @Column({ length: 255 })
  @Field()
  last_name: string;

  @Column({ nullable: false, length: 255 })
  @Field()
  email: string;

  @Column({ length: 255 })
  @Field()
  phone: string;

  @Column()
  @Field()
  transactions: number;

  @Column({ nullable: false, default: 'ap', length: 255 })
  @Field()
  tier: string;

  @Column({ type: 'timestamp without time zone', default: 'NOW()' })
  @Field()
  last_seen: Date;

  @Column({ nullable: false, length: 255 })
  @Field()
  password: string;

  @Column({ type: 'timestamp without time zone', default: 'NOW()' })
  @Field()
  inserted_at: Date;

  @Column({ type: 'timestamp without time zone', default: 'NOW()' })
  @Field()
  updated_at: Date;

  @Column({ nullable: false, length: 255 })
  @Field()
  provider: string;

  // @Column({ type: 'jsonb', array:false, default: [] })
  // @Field(() => [])
  // recent_viewed: [];

  // @Column({ type: 'jsonb', default: [] })
  // @Field(() => [Any])
  // favourites: [];

  // @Column({ type: 'jsonb', default: [] })
  // @Field(() => [])
  // products_saved_for_later: [];

  @Column({ default: false })
  @Field()
  is_alliance: boolean;

  @Column({ length: 255 })
  @Field()
  alliance_id: string;

  @Column({ length: 255 })
  @Field()
  oauth_id: string;

  @Column({ length: 11 })
  @Field()
  phone_country_code: string;

  @Column()
  @Field()
  city_id: number;

  @Column()
  @Field()
  country_id: number;

  @Column()
  @Field()
  news_letter_subscribed: boolean;

  @Column({ length: 255 })
  @Field()
  vat_number: string;

  @Column()
  @Field()
  vat_registration_date: Date;
}
