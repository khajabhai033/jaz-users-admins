import { Module } from '@nestjs/common';
import { GraphQLModule, GraphQLFederationModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UsersModule,
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      playground: true,
      path: '/api/users',
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
