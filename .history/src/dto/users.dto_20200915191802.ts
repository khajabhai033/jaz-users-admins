import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UsersDto {
  @Field(() => ID)
  id: number;

  @Field()
  client_id: string;

  @Field()
  active: boolean;

  @Field()
  title: string;

  @Field()
  first_name: string;

  @Field()
  last_name: string;

  @Field()
  email: string;

  @Field()
  phone: string;

  @Field()
  transactions: number;

  @Field()
  tier: string;

  @Field()
  last_seen: Date;

  @Field()
  password: string;

  @Field()
  inserted_at: Date;

  @Field()
  updated_at: Date;

  @Field()
  provider: string;

  @Field()
  recent_viewed: [];

  @Field()
  favourites: [];

  @Field()
  products_saved_for_later: [];

  @Field()
  is_alliance: boolean;

  @Field()
  alliance_id: string;

  @Field()
  oauth_id: string;

  @Field()
  phone_country_code: string;

  @Field()
  city_id: number;

  @Field()
  country_id: number;

  @Field()
  news_letter_subscribed: boolean;

  @Field()
  vat_number: string;

  @Field()
  vat_registration_date: Date;
}
