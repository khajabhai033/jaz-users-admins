import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UsersDto {
  @Field(() => ID)
  id: number;

  @Field()
  client_id: string;

  @Field()
  active: boolean;

  @Field()
  title: string;

  @Field()
  first_name: string;

  @Field()
  last_name: string;

  @Field()
  email: string;

  @Field()
  phone: string;

  @Field()
  transactions: number;

  @Field({ nullable: false, default: 'ap' })
  tier: string;

  @Field({ type: 'timestamp', default: Date.now() })
  last_seen: Date;

  @Field({ nullable: false })
  password: string;

  @Field({ type: 'timestamp', default: Date.now() })
  inserted_at: Date;

  @Field({ type: 'timestamp', default: Date.now() })
  updated_at: Date;

  @Field({ nullable: false })
  provider: string;

  @Field({ type: 'jsonb', default: [] })
  recent_viewed: [];

  @Field({ type: 'jsonb', default: [] })
  favourites: [];

  @Field({ type: 'jsonb', default: [] })
  products_saved_for_later: [];

  @Field({ default: false })
  is_alliance: boolean;

  @Field()
  alliance_id: string;

  @Field()
  oauth_id: string;

  @Field()
  phone_country_code: string;

  @Field()
  city_id: number;

  @Field()
  country_id: number;

  @Field()
  news_letter_subscribed: boolean;

  @Field()
  vat_number: string;

  @Field()
  vat_registration_date: Date;
}
