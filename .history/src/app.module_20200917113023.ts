import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UsersModule,
    GraphQLModule.forRoot({ autoSchemaFile: true, playground: true }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
