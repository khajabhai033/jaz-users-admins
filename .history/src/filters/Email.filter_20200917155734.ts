import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class EmailFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    console.log(response);
    console.log(request);
    console.log(exception);

    // response.statusCode = exception.getStatus();

    response.json({
      statusCode: status,
      message: exception.message,
      timestamp: new Date().toISOString(),
      //   path: request.url,
    });

    // response.status(status).json({
    //   statusCode: 400,
    //   error: 'Bad Request',
    //   message: [
    //     {
    //       target: {},
    //       property: 'email',
    //       children: [],
    //       constraints: {
    //         isEmail: 'email must be an email',
    //       },
    //     },
    //     // other field exceptions
    //   ],
    // });
  }
}
