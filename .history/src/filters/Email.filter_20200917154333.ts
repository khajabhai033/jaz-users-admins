import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';

@Catch()
export class EmailFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status = exception.getStatus();

    // response.status = status;
    response.message = exception.message;
    // response.status(status).json({
    //   statusCode: 400,
    //   error: 'Bad Request',
    //   message: [
    //     {
    //       target: {},
    //       property: 'email',
    //       children: [],
    //       constraints: {
    //         isEmail: 'email must be an email',
    //       },
    //     },
    //     // other field exceptions
    //   ],
    // });
  }
}
