import { Module } from '@nestjs/common';
import { GraphQLModule, GraphQLFederationModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AdminsModule } from './admins/admins.module';
import { typeOrmConfig } from './config/typeOrm.config';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    UsersModule,
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      playground: true,
      path: '/api/users',
    }),
    AdminsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
