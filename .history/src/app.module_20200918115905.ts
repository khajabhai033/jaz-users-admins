import { Module } from '@nestjs/common';
import { GraphQLModule, GraphQLFederationModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AdminsModule } from './admins/admins.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UsersModule,
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      playground: true,
      path: '/api/admin-panel',
    }),
    AdminsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
