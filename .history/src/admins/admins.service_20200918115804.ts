import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admins } from '../models/admins.entity';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(Admins) private readonly adminRepo: Repository<Admins>,
  ) {}

  public async getAdmins(): Promise<Admins[]> {
    return await this.adminRepo.find();
  }
}
