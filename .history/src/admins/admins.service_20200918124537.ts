import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admins } from '../models/admins.entity';
import { AdminsCreateInput } from './inputs/admin.input';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(Admins) private readonly adminRepo: Repository<Admins>,
  ) {}

  public async getAdmins(): Promise<Admins[]> {
    return await this.adminRepo.find({
      order: { first_name: 'ASC' },
    });
  }

  public async createAdmin(admin: AdminsCreateInput): Promise<Admins> {
    const newAdmin = new Admins();
    Object.assign(newAdmin, admin);

    return await this.adminRepo.save(newAdmin);
  }
}
