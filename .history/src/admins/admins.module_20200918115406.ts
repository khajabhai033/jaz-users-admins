import { Module } from '@nestjs/common';
import { AdminsService } from './admins.service';
import { AdminsResolver } from './admins.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Admins } from '../models/admins.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Admins])],
  providers: [AdminsService, AdminsResolver],
})
export class AdminsModule {}
