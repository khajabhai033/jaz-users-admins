import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Admins } from '../models/admins.entity';
import { AdminsService } from './admins.service';
import { AdminsCreateInput } from '../inputs/admin.input';
import { AdminsUpdateInput } from '../inputs/admin.Update.input';

@Resolver(() => Admins)
export class AdminsResolver {
  constructor(private readonly adminService: AdminsService) {}

  @Query(() => [Admins])
  public async admins(): Promise<Admins[]> {
    return await this.adminService.getAdmins();
  }

  @Query(() => Admins)
  public async admin(@Args('email') email: string): Promise<Admins> {
    return await this.adminService.findAdmin({ email });
  }

  @Mutation(() => Admins)
  public async addAdmin(
    @Args('admin') admin: AdminsCreateInput,
  ): Promise<Admins> {
    return this.adminService.createAdmin(admin);
  }

  @Mutation(() => Admins)
  public async updateAdmin(
    @Args('email') email: string,
    @Args('admin') admin: AdminsUpdateInput,
  ): Promise<Admins> {
    return this.adminService.updateAdmin(email, admin);
  }
}
