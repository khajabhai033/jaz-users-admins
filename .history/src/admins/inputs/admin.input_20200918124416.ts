import { Field, Int, InputType } from '@nestjs/graphql';
import { AdminPermission } from '../dto/adminPermissions.dto';

@InputType()
export class AdminsCreateInput {
  @Field()
  first_name: string;

  @Field()
  last_name: string;

  @Field()
  email: string;

  @Field({ defaultValue: true })
  active: boolean;

  @Field()
  password: string;

  @Field()
  inserted_at: Date;

  @Field()
  updated_at: Date;

  @Field({ nullable: true })
  webhook_token: string;

  @Field({ nullable: true })
  superadmin: boolean;

  @Field(() => AdminPermission, { nullable: true })
  permissions: AdminPermission;

  @Field(() => [Int])
  cities_area_ids: any;
}
