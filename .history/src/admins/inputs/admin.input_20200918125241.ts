import { Field, Int, InputType } from '@nestjs/graphql';
import { AdminPermission } from '../dto/adminPermissions.dto';
import { IsNotEmpty, IsEmail, MinLength } from 'class-validator';

@InputType()
export class AdminsCreateInput {
  @Field()
  @IsNotEmpty()
  first_name: string;

  @Field()
  @IsNotEmpty()
  last_name: string;

  @Field()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field({ defaultValue: true })
  active: boolean;

  @Field()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @Field()
  @IsNotEmpty()
  inserted_at: Date;

  @Field()
  @IsNotEmpty()
  updated_at: Date;

  @Field({ nullable: true })
  webhook_token: string;

  @Field({ nullable: true })
  superadmin: boolean;

  //   @Field(() => AdminPermission, { nullable: true })
  //   permissions: AdminPermission;

  @Field(() => [Int])
  @IsNotEmpty()
  cities_area_ids: any;
}
