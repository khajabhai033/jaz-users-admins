import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admins } from '../models/admins.entity';
import { AdminsCreateInput } from '../inputs/admin.input';
import { AdminsUpdateInput } from '../inputs/admin.Update.input';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(Admins) private readonly adminRepo: Repository<Admins>,
  ) {}

  public async getAdmins(): Promise<Admins[]> {
    return await this.adminRepo.find({
      order: { first_name: 'ASC' },
    });
  }

  public async createAdmin(admin: AdminsCreateInput): Promise<Admins> {
    const newAdmin = new Admins();
    Object.assign(newAdmin, admin);
    newAdmin.password = await bcrypt.hash(newAdmin.password, 10);
    return await this.adminRepo.save(newAdmin);
  }

  public async updateAdmin(
    email: string,
    admin: AdminsUpdateInput,
  ): Promise<Admins> {
    const ad = await this.findAdmin({ email });

    Object.assign(ad, admin);

    return await this.adminRepo.save(ad);
  }

  public async deleteAdmin(email: string): Promise<Admins | String> {
    const admin = await this.findAdmin({ email });

    if ((await this.adminRepo.remove(admin)) != null) return admin;
  }

  public async findAdmin(filter): Promise<Admins> {
    const admin = await this.adminRepo.findOne(filter);

    if (!admin)
      throw new NotFoundException(
        `Admin doesn\'t exist with the given criteria [${filter.email}]`,
      );

    return admin;
  }
}
