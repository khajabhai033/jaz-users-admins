import { Field, ObjectType } from '@nestjs/graphql';

class Options {
  readonly: boolean;
  store_ids: number[];
}

@ObjectType()
class Orders {
  @Field()
  active: boolean;
  @Field(() => Options)
  options: Options;
}

@ObjectType()
export class AdminPermission {
  @Field()
  orders: Orders;
}
