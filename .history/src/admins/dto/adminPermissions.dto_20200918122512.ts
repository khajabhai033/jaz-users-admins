import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AdminPermission {
  @Field()
  orders: Orders;
}

class Orders {
  active: boolean;
  options: [];
}
