import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AdminPermission {
  @Field()
  orders: Orders;
}

@ObjectType()
class Orders {
  active: boolean;
  options: Options;
}

class Options {
  readonly: boolean;
  store_ids: number[];
}
