import { Field, Int, InputType } from '@nestjs/graphql';

@InputType()
class Options {
  @Field({ nullable: true })
  country: string;

  @Field()
  read_only: boolean;
  @Field(() => [Int])
  store_ids: number[];
}

@InputType()
class Orders {
  @Field()
  active: boolean;
  @Field(() => Options)
  options: Options;
}

@InputType()
export class AdminPermission {
  @Field()
  orders: Orders;
}
