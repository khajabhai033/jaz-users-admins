import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
class Orders {
  active: boolean;
  options: Options;
}

@ObjectType()
export class AdminPermission {
  @Field()
  orders: Orders;
}

class Options {
  readonly: boolean;
  store_ids: number[];
}
