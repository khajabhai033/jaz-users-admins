import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Admins } from '../models/admins.entity';
import { AdminsService } from './admins.service';
import { AdminsCreateInput } from './inputs/admin.input';

@Resolver(() => Admins)
export class AdminsResolver {
  constructor(private readonly adminService: AdminsService) {}

  @Query(() => [Admins])
  public async admins(): Promise<Admins[]> {
    return await this.adminService.getAdmins();
  }

  @Mutation(() => Admins)
  public async addAdmin(
    @Args('admin') admin: AdminsCreateInput,
  ): Promise<Admins> {
    return this.adminService.createAdmin(admin);
  }
}
