import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admins } from '../models/admins.entity';
import { AdminsCreateInput } from './inputs/admin.input';
import { AdminsUpdateInput } from './inputs/admin.Update.input';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(Admins) private readonly adminRepo: Repository<Admins>,
  ) {}

  public async getAdmins(): Promise<Admins[]> {
    return await this.adminRepo.find({
      order: { first_name: 'ASC' },
    });
  }

  public async createAdmin(admin: AdminsCreateInput): Promise<Admins> {
    const newAdmin = new Admins();
    Object.assign(newAdmin, admin);

    return await this.adminRepo.save(newAdmin);
  }

  public async updateAdmin(
    email: string,
    admin: AdminsUpdateInput,
  ): Promise<Admins> {}

  public async findAdmin(filter): Promise<Admins> {
    const admin = await this.adminRepo.findOne(filter);

    if (!admin)
      throw new NotFoundException(
        `Admin doesn\'t exist with the given criteria [${filter}]`,
      );

    return admin;
  }
}
