import { Query, Resolver } from '@nestjs/graphql';
import { Admins } from '../models/admins.entity';
import { AdminsService } from './admins.service';

@Resolver(() => Admins)
export class AdminsResolver {
  constructor(private readonly adminService: AdminsService) {}

  @Query(() => [Admins])
  public async admins(): Promise<Admins[]> {}
}
