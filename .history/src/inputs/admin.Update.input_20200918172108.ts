import { Field, Int, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsEmail, MinLength, IsOptional } from 'class-validator';
import { AdminPermissionInput } from './adminPermissions.input';

@InputType()
export class AdminsUpdateInput {
  @Field({ nullable: true })
  @IsOptional()
  @IsNotEmpty()
  first_name: string;

  @Field({ nullable: true })
  @IsOptional()
  last_name: string;

  @Field({ nullable: true })
  @IsOptional()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field({ defaultValue: true, nullable: true })
  @IsOptional()
  active: boolean;

  @Field({ nullable: true })
  @IsOptional()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @Field({ nullable: true })
  @IsOptional()
  @IsNotEmpty()
  inserted_at: Date;

  @Field({ nullable: true })
  @IsOptional()
  @IsNotEmpty()
  updated_at: Date;

  @Field({ nullable: true })
  @IsOptional()
  webhook_token: string;

  @Field({ nullable: true })
  @IsOptional()
  superadmin: boolean;

  @Field(() => AdminPermissionInput, { nullable: true })
  @IsOptional()
  permissions: AdminPermissionInput;

  @Field(() => [Int], { nullable: true })
  @IsOptional()
  @IsNotEmpty()
  cities_area_ids: any;
}
