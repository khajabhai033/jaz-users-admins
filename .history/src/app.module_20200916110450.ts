import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UsersModule,
    GraphQLModule.forRoot({ autoSchemaFile: 'schema.gql', playground: true }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
