import { Field, ID, InputType } from '@nestjs/graphql';
import { IsEmail, IsEmpty } from 'class-validator';
import { UserProductDto } from '../users/dto/user.products.dto';

@InputType()
export class UserUpdate {
  @Field({ nullable: true })
  client_id: string;

  @Field({ nullable: true })
  active: boolean;

  @Field({ nullable: true })
  title: string;

  @Field({ nullable: true })
  first_name: string;

  @Field({ nullable: true })
  last_name: string;

  @Field({ nullable: false })
  @IsEmpty()
  @IsEmail()
  email: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  transactions: number;

  // @Field({ nullable: false, defaultValue: 'ap' })
  @Field({ nullable: false })
  @IsEmpty()
  tier: string;

  @Field({ nullable: true })
  last_seen: Date;

  @Field({ nullable: false })
  @IsEmpty()
  password: string;

  // @Field({ nullable: false })
  // @IsEmpty()
  // inserted_at: Date;

  @Field({ nullable: false, defaultValue: new Date() })
  @IsEmpty()
  updated_at: Date;

  @Field({ nullable: true, defaultValue: 'password' })
  provider: string;

  @Field(() => [UserProductDto], { nullable: true })
  recent_viewed: UserProductDto[];

  @Field(() => [UserProductDto], { nullable: true })
  favourites: UserProductDto[];

  @Field(() => [UserProductDto], { nullable: true })
  products_saved_for_later: UserProductDto[];

  @Field({ nullable: true, defaultValue: false })
  is_alliance: boolean;

  @Field({ nullable: true })
  alliance_id: string;

  @Field({ nullable: true })
  oauth_id: string;

  @Field({ nullable: true })
  phone_country_code: string;

  @Field({ nullable: true })
  city_id: number;

  @Field({ nullable: true })
  country_id: number;

  @Field({ nullable: true })
  news_letter_subscribed: boolean;

  @Field({ nullable: true })
  vat_number: string;

  @Field({ nullable: true })
  vat_registration_date: Date;
}
