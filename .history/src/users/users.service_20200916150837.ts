import { Injectable } from '@nestjs/common';
import { Users } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private readonly userRepo: Repository<Users>,
  ) {}

  async getAllUsers(): Promise<Users[]> {
    return await this.userRepo.find();
  }

  async createUser(user: Users): Promise<String> {
    const newUser = new Users();
    Object.assign(newUser, user);
    try {
      await this.userRepo.save(newUser);
    } catch {
      err => {
        throw new Error();
      };
    }
    return '';
  }
}
