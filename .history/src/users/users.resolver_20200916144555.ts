import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { Users } from '../models/users.entity';
import { UsersService } from './users.service';

@Resolver(() => Users)
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query(() => [Users])
  public async users(): Promise<Users[]> {
    return this.userService.getAllUsers();
  }

  @Mutation(() => String)
  public async signUp(@Args('user') user: Users): Promise<String> {
    return this.userService.createUser(user);
  }
}
