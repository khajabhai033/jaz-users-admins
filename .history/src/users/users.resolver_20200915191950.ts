import { Query } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';
import { UsersDto } from 'src/dto/users.dto';
import { UsersService } from './users.service';

@Resolver(() => UsersDto)
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query(() => [UsersDto])
  public async users(): Promise<UsersDto[]> {
    return this.userService.getAllUsers();
  }
}
