import { Injectable } from '@nestjs/common';
import { Users } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.input';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private readonly userRepo: Repository<Users>,
  ) {}

  async getAllUsers(): Promise<Users[]> {
    return await this.userRepo.find();
  }

  async createUser(user: User): Promise<Users> {
    const newUser = new Users();
    Object.assign(newUser, user);
    try {
      return await this.userRepo.save(newUser);
    } catch {
      err => {
        throw err;
      };
    }
    // return 'User signup success';
  }

  async checkUser(email, password): Promise<String> {
    const result = await this.userRepo.findAndCount({
      email,
      password,
    });
    console.log(result[1]);
    return '';
  }
}
