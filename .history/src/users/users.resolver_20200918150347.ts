import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { User } from '../inputs/user.input';
import { UserUpdate } from '../inputs/user.update.input';
import { Users } from '../models/users.entity';
import { UsersService } from './users.service';
import { UseFilters } from '@nestjs/common';
import { EmailFilter } from 'src/filters/Email.filter';

@Resolver(() => Users)
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query(() => [Users])
  public async users(): Promise<Users[]> {
    return this.userService.getAllUsers();
  }

  @Query(() => String)
  public async signIn(
    @Args('email') email: string,
    @Args('password') password: string,
  ): Promise<string> {
    return this.userService.checkUser(email, password);
  }

  @Mutation(() => Users)
  @UseFilters(EmailFilter)
  public async signUp(@Args('user') user: User): Promise<Users> {
    return this.userService.createUser(user);
  }

  /**
   * updateUser
   */
  @Mutation(() => Users)
  public async updateUser(
    @Args('token') token: string,
    @Args('email') email: string,
    @Args('user') user: UserUpdate,
  ): Promise<Users> {
    return this.userService.updateUser(token, email, user);
  }

  /**
   * deleteUser
   */
  @Mutation(() => Users)
  public deleteUser(@Args('email') email: string): Promise<Users> {
    return this.userService.deleteUser(email);
  }
}
