import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { User } from 'src/models/user.input';
import { Users } from '../models/users.entity';
import { UsersService } from './users.service';

@Resolver(() => Users)
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query(() => [Users])
  public async users(): Promise<Users[]> {
    return this.userService.getAllUsers();
  }

  @Mutation(() => Users)
  public async signUp(@Args('user') user: User): Promise<Users> {
    return this.userService.createUser(user);
  }

  @Query(() => Users)
  public async signIn(
    @Args('email') email: string,
    @Args('password') password: string,
  ): Promise<User> {
    return this.userService.checkUser(email, password);
  }
}
