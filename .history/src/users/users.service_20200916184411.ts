import {
  Injectable,
  NotFoundException,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { Users } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.input';
import { AuthGuard } from 'src/guards/auth.guard';
import { UserUpdate } from 'src/models/user.update.input';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private readonly userRepo: Repository<Users>,
  ) {}

  async getAllUsers(): Promise<Users[]> {
    return await this.userRepo.find();
  }

  async createUser(user: User): Promise<Users> {
    const newUser = new Users();
    Object.assign(newUser, user);
    newUser.password = await bcrypt.hash(newUser.password, 10);
    return await this.userRepo.save(newUser);
    // return 'User signup success';
  }

  async checkUser(email, password): Promise<Users> {
    const result = await this.userRepo.findOne({
      email,
    });

    if (!result) throw new NotFoundException('Email or Password is invalid');
    if ((await bcrypt.compare(password, result.password)) == false)
      return result;
    throw new BadRequestException('Incorrect Password');
    return;
  }

  @UseGuards(AuthGuard)
  async updateUser(email, user: UserUpdate): Promise<Users> {
    const result = await this.userRepo.findOne({ email });
    if (!result) throw new NotFoundException('Email is invalid');
    Object.assign(result, user);
    return await this.userRepo.save(result);
  }

  @UseGuards(AuthGuard)
  async deleteUser(email): Promise<Users> {
    const result = await this.userRepo.findOne({ email });
    if (!result) throw new NotFoundException('Email is invalid');
    return await this.userRepo.remove(result);
  }
}
