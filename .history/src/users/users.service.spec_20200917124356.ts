import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { Users } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;
  // let repo: Repository<Users>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(), TypeOrmModule.forFeature([Users])],
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
    console.log(service);
    // repo = module.get<Repository<Users>>(getRepositoryToken(Users));
  });

  it('should be defined', () => {
    // expect(service).toBeDefined();
  });
});
