import { Injectable, NotFoundException, UseGuards } from '@nestjs/common';
import { Users } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.input';
import { AuthGuard } from 'src/guards/auth.guard';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private readonly userRepo: Repository<Users>,
  ) {}

  async getAllUsers(): Promise<Users[]> {
    return await this.userRepo.find();
  }

  async createUser(user: User): Promise<Users> {
    const newUser = new Users();
    Object.assign(newUser, user);
    try {
      return await this.userRepo.save(newUser);
    } catch {
      err => {
        throw err;
      };
    }
    // return 'User signup success';
  }

  async checkUser(email, password): Promise<Users> {
    const result = await this.userRepo.findOne({
      email,
      password,
    });
    if (!result) throw new NotFoundException('Email or Password is invalid');
    return result;
  }

  @UseGuards(AuthGuard)
  async updateUser(email, user): Promise<Users> {
    const result = await this.userRepo.findOne({ email });
    if (!result) throw new NotFoundException('Email is invalid');
    Object.assign(result, user);
    return await this.userRepo.save(result);
  }
}
