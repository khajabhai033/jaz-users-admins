import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UserProductInput {
  @Field({ nullable: true })
  id: number;

  @Field({ nullable: true })
  type: string;
}
