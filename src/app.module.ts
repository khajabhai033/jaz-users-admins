import { Module } from '@nestjs/common';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AdminsModule } from './admins/admins.module';
import { typeOrmConfig } from './config/typeOrm.config';
import * as config from 'config';

const graphqlConf = config.get('graphql');

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    UsersModule,
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      playground: process.env.playground || graphqlConf.playground,
      path: '/api/users',
    }),
    AdminsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
