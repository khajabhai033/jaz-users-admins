import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class UserProductDto {
  @Field({ nullable: true })
  id: number;

  @Field({ nullable: true })
  type: string;
}
